#!/usr/bin/env bash
podman-remote system connection remove podman-host
podman-remote system connection add --default podman-host tcp://podman-host:1337
